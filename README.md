# Java EE demo

https://github.com/quarkusio/quarkus-quickstarts/tree/master/hibernate-orm-panache-quickstart was used as the starter for this project to verify correct packages etc.
Practically nothing is left of the original project.

## Assignment goals

*[x] Contains database table for customers (1.)
    * Columns:
        * Unique ID
        * Customer identifier (text)
        * Name (Text)
        
*[x] Contains customer tasks for the customer that contains reference to the customer (2.)
    * Columns:
        * Unique ID for the task
        * Textual representation of task
        * flag for done / not done
        
*[x] Clear database table contents and create at least 5 sample entries for customer, and 3 events for any number of 
     customers (your choice) in the database on program start. (3.)
     
*[x] Create RESTful API in Java EE for the said database tables, and an interface that lists both customer details and
     the task for customer in single call. (4.)
     
*[x] Create a messaging queue with technology of your choice (for example using a container), and send audit message to 
     that queue on each call to the REST api (5.)
     
*[x] On each RESTful API call, the backend sends an audit message to the queue that indicates the HTTP verb performed 
     against the backend. (6.)

## Run the whole assignment

Make sure you have docker running and run ```docker-compose up``` from the parent directory. Note that there are no build scripts etc. The backend and 
frontend is ran in dev mode. Be patiented since the frontend container does a ```yarn install``` which takes some time.

Site is available at http://localhost:3000. You should see a message coming back from Kafka in the logs after requests.

You can also just spin up the kafka, zookeeper and db containers with ```docker-compose up``` in the backend directory 
and then running ```./mvnw quarkus:dev``` for the backend and ```yarn install``` and ```yarn start``` for the frontend 
in the frontend directory.

### Tests

Make sure you have docker running and run ```./mvnw test``` in the backend directory.

* There are only integration tests since there is no real application logic to test with unit tests
* Tests should cover each REST endpoint
* Tests spin up their own containers for the DB and Messaging. 

## Backend

### Developing / running
* Make sure PostgreSQL is running. You can spin it up by running ```docker-compose up``` in the backend directory.
* Hot reload capable
* Run ```./mvnw quarkus:dev``` in the backend directory to start

### JavaEE
* Quarkus (https://quarkus.io) was selected, since it was praised for its performance and seemed 
  like a good future technology to learn. There is a Architectural Decision Record of this in the 
  'ADRs' directory.

### Database / ORM
* Hibernate Panache (https://quarkus.io/guides/hibernate-orm-panache) is used with JPA (provided by Quarkus)
* Uses PostgreSQL as the DB
* Database initialization is done in a custom DbInit class

### Messaging
* Apache Kafka running in a container
* Backend sends messages with reactive messaging. Also receives them back and logs them.

## Frontend

The frontend is implemented with React using Material UI. Just a very simple table component with collapse, nothing
fancy.