package com.martinsen.javaee.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Outgoing;

import javax.enterprise.context.ApplicationScoped;

@Slf4j
@ApplicationScoped
public class MessagingService {

    @Incoming("rest-event") // This is the in-memory message channel
    @Outgoing("rest-messages-out") // This is the outgoing kafka message channel
    public Message<String> sendMessage(String message) {
        log.debug("RECEIVED MESSAGING EVENT FROM REST!");
        return Message.of(message);
    }

    @Incoming("messages") // This is the incoming kafka message channel
    public void consumeMessage(String message) {
        log.info(" ++++ HEARD BACK FROM KAFKA: " + message + " ++++");
    }
}
