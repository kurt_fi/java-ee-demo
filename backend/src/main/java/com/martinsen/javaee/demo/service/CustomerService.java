package com.martinsen.javaee.demo.service;

import com.martinsen.javaee.demo.model.Customer;
import com.martinsen.javaee.demo.model.Task;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * This service layer is simple and exists only to separate REST code from the selected DB abstraction layer.
 */
@ApplicationScoped
public class CustomerService {

    @PersistenceContext
    EntityManager entityManager;

    public List<Customer> getCustomers() {
        return Customer.listAll(Sort.by("name"));
    }

    public Optional<Customer> getCustomer(Long id) {
        return Customer.findByIdOptional(id);
    }

    @Transactional
    public void createCustomer(Customer customer) {
        customer.persist();
    }

    @Transactional
    public void editCustomer(Customer customer) {
        // Does not work due to duplicate id
        //Customer.persist(customer);

        // Not happy about this, but will do for now
        Customer entity = Customer.findById(customer.id);
        entity.updateFields(customer);
    }

    @Transactional
    public void deleteCustomer(Long id) {
        Customer.deleteById(id);
    }

    @Transactional
    public void createTask(Customer customer, Task task) {
        task.setCustomer(customer);
        customer.getTasks().add(task);
    }

}
