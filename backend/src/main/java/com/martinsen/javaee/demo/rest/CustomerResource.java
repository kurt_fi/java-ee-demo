package com.martinsen.javaee.demo.rest;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.martinsen.javaee.demo.model.Customer;
import com.martinsen.javaee.demo.model.Task;
import com.martinsen.javaee.demo.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Slf4j
@Path("customers")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class CustomerResource {

    @Inject
    CustomerService customerService;

    @Inject
    @Channel("rest-event")
    Emitter<String> messaging;

    @GET
    public List<Customer> getAll() {
        messaging.send("GET");
        return customerService.getCustomers();
    }

    @GET
    @Path("{id}")
    public Customer getOne(@PathParam Long id) {
        messaging.send("GET");
        log.info("GET customer with id " + id);

        Optional<Customer> entity = customerService.getCustomer(id);
        return entity.orElseThrow(() -> new WebApplicationException("Customer with id of " + id + " does not exist.", 404));
    }

    @POST
    public Response postOne(Customer customer) {
        messaging.send("POST");
        log.info("POST customer");

        if (customer.id != null) {
            throw new WebApplicationException("Id was invalidly set on request.", 422);
        }

        customerService.createCustomer(customer);
        return Response.ok(customer).status(201).build();
    }

    @PUT
    @Path("{id}")
    public Response putOne(@PathParam Long id, Customer customer) {
        messaging.send("PUT");
        log.info("PUT customer with id " + id);

        /* This should not be possible
        if (id == null) {
            throw new WebApplicationException("Customer id not given", 400);
        }
        */
        if (customer == null) {
            throw new WebApplicationException("Empty body for customer of id " + id + ".", 422);
        }
        if (!id.equals(customer.id)) {
            throw new WebApplicationException("Customer has different id " + customer.id + " than the request id " + id + ".", 422);
        }
        if (Customer.findByIdOptional(id).isEmpty()) {
            throw new WebApplicationException("Customer with id of " + id + " does not exist.", 404);
        }

        customerService.editCustomer(customer);
        return Response.ok(customer).status(200).build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteOne(@PathParam Long id) {
        messaging.send("DELETE");
        log.info("DELETE customer with id " + id);

        Optional<Customer> optional = Customer.findByIdOptional(id);
        Customer entity = optional.orElseThrow(
                () -> new WebApplicationException("Customer with id of " + id + " does not exist.", 404)
        );

        customerService.deleteCustomer(id);
        return Response.status(204).build();
    }

    @POST
    @Path("{customerId}/tasks")
    public Response postOne(@PathParam Long customerId, Task task) {
        messaging.send("POST");
        log.info("POST task for customer id " + customerId);
        Optional<Customer> optional = customerService.getCustomer(customerId);
        Customer customer = optional.orElseThrow(
                () -> new WebApplicationException("Customer with id of " + customerId + " does not exist.", 404)
        );

        customerService.createTask(customer, task);
        return Response.ok(task).status(201).build();
    }

    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Override
        public Response toResponse(Exception exception) {
            log.error("Error while processing customer(s)", exception);

            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }
            return Response.status(code)
                    .entity(Json.createObjectBuilder().add("error", exception.getMessage()).add("code", code).build())
                    .build();
        }

    }
}
