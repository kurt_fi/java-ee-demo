package com.martinsen.javaee.demo;

import com.martinsen.javaee.demo.model.Customer;
import com.martinsen.javaee.demo.model.Task;
import io.quarkus.runtime.StartupEvent;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * This class was created because the Panache id strategy and import.sql were not playing nice.
 * Since then it was discovered that extending PanacheEntityBase instead of PanacheEntity will gives control over id
 * strategy. We're still keeping it for now.
 */
@Slf4j
@ApplicationScoped
public class DbInit {

    @ConfigProperty(name = "javaeedemo.startup.db.prefill")
    boolean preFillEntities = false;

    @ConfigProperty(name = "javaeedemo.startup.db.prefill.customers.size")
    int numberOfCustomersToCreate = 1;

    @ConfigProperty(name = "javaeedemo.startup.db.prefill.tasks.size")
    int numberOfTasksToCreatePerCustomer = 1;

    /**
     * Listens to the startup event and initializes the database entities if the parameter is set in the config file.
     * This is used when import.sql cannot be used due to ids and joined tables.
     * @param ev
     */
    @Transactional
    void init(@Observes StartupEvent ev) {
        log.info("* Custom init started! *");
        if (preFillEntities) {
            log.info("* Pre-filling entities... *");
            List<Long> namePostfixes = LongStream.rangeClosed(1, numberOfCustomersToCreate).boxed()
                    .collect(Collectors.toList());
            List<Customer> customers = namePostfixes.stream().map(postfix ->
                createCustomerWithTasks(postfix.toString(), UUID.randomUUID().toString())
            ).collect(Collectors.toList());

            Customer.persist(customers);
        }
    }

    private Customer createCustomerWithTasks(String custNamePostfix, String uniqueId) {
        Random random = new Random();

        Customer customer = Customer.builder()
                .customerId(uniqueId)
                .name("Customer " + custNamePostfix)
                .build();

        List<Long> namePostfixes = LongStream.rangeClosed(1, numberOfTasksToCreatePerCustomer).boxed()
                .collect(Collectors.toList());
        List<Task> tasks = namePostfixes.stream().map(postfix ->
                createTask(customer, custNamePostfix + "." + postfix, random.nextBoolean())
        ).collect(Collectors.toList());

        customer.setTasks(tasks);

        return customer;
    }

    private Task createTask (Customer customer, String taskNamePostfix, boolean isDone) {
        return Task.builder()
                .customer(customer)
                .description("Task " + taskNamePostfix)
                .done(isDone)
                .build();
    }

}
