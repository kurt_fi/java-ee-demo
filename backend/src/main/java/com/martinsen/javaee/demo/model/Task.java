package com.martinsen.javaee.demo.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.*;

// JPA
@Entity
@Cacheable
// Lombok
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class Task extends PanacheEntity {

    @Column(length = 2000)
    public String description;

    @Column(nullable = false)
    @Builder.Default
    public Boolean done = false;

    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    @JsonbTransient // We want to prevent circular references
    public Customer customer;
}
