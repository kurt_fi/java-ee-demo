package com.martinsen.javaee.demo.model;

import javax.persistence.*;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.*;

import java.util.List;

// JPA
@Entity
@Cacheable
// Lombok
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class Customer extends PanacheEntity {

    // public variables!? Crazy, but best practise in Panache

    @Column(nullable = false, unique = true)
    public String customerId;

    @Column(nullable = false)
    public String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
    public List<Task> tasks;

    /**
     * Due to some ORM related issues this seemed to be a quick working solution.
     * @param customer
     */
    public void updateFields(Customer customer) {
        this.setCustomerId(customer.getCustomerId());
        this.setName(customer.getName());
        if (customer.getTasks() != null) {
            this.setTasks(customer.getTasks());
        }
    }
}
