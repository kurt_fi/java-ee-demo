package com.martinsen.javaee.demo.rest;

import static io.restassured.RestAssured.given;

import com.martinsen.javaee.demo.model.Customer;
import com.martinsen.javaee.demo.model.Task;
import com.martinsen.javaee.demo.service.CustomerService;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.Matchers.*;

@Slf4j
@QuarkusTest
@QuarkusTestResource(KafkaResource.class)
public class CustomerResourceTest {

    @Inject
    CustomerService customerService;

    /* Moved to application.properties using JDBC url
    @Container
    static PostgreSQLContainer db = new PostgreSQLContainer<>("postgres:11-alpine")
            .withDatabaseName("quarkus_test")
            .withUsername("quarkus_test")
            .withPassword("quarkus_test")
            .withExposedPorts(5432)
            // Below should not be used - Function is deprecated and for simplicity of test , You should override your properties at runtime
            .withCreateContainerCmdModifier(cmd -> {
                cmd
                        .withHostName("localhost")
                        .withPortBindings(new PortBinding(Ports.Binding.bindPort(5432), new ExposedPort(5432)));
            });
    */

    @Test
    public void whenGetAll_thenOkAndNotEmptyAndContainsTask() {
        //List all, should have some entries:
        given()
                .when().get("/customers")
                .then()
                .statusCode(200)
                .assertThat().body("size()", greaterThan(0))
                .assertThat().body(containsString("Task "))
        .log().all();
    }

    @Test
    public void whenGetOne_thenOkAndNotEmpty() {
        Customer customer = customerService.getCustomers().get(0);

        //List all, should have some entries:
        given()
                .when().get("/customers/" + customer.id)
                .then()
                .statusCode(200);
    }

    @Test
    public void whenDeleteOne_thenNoContentAndNotFound() {
        // Get first customer found
        Customer customer = customerService.getCustomers().get(0);
        long idToDelete = customer.id;

        //Delete
        given()
                .when().delete("/customers/" + idToDelete)
                .then()
                .statusCode(204);

        //Get by id
        given()
                .when().get("/customers/" + idToDelete)
                .then()
                .statusCode(404);
    }

    @Test
    public void whenPostOne_thenCreated() {
        Jsonb jsonb = JsonbBuilder.create();

        Customer customer = Customer.builder()
                .customerId(UUID.randomUUID().toString())
                .name("Added customer").build();

        //Post
        given()
                .log().all()
                .contentType("application/json")
                .with().body(jsonb.toJson(customer))
                .when().post("/customers")
                .then()
                .statusCode(201);
    }

    @Test
    public void whenPutOne_thenOkAndGetBodyContainsName() {
        Jsonb jsonb = JsonbBuilder.create();

        String expectedEditedName = "Edited customer name";

        // Get first customer found
        Customer customer = customerService.getCustomers().get(0);
        customer.setName(expectedEditedName);

        //Post
        given()
                .log().all()
                .contentType("application/json")
                .with().body(jsonb.toJson(customer))
                .when().put("/customers/" + customer.id)
                .then()
                .statusCode(200);

        //Get by id
        given()
                .when().get("/customers/" + customer.id)
                .then()
                .statusCode(200)
                .assertThat()
                .body(
                        containsString(expectedEditedName)
                );
    }

    @Test
    public void whenPostOneTask_thenCreatedAndCustomerBodyContainsTask() {
        Jsonb jsonb = JsonbBuilder.create();

        // Get first customer found
        Customer customer = customerService.getCustomers().get(0);
        Task task = Task.builder()
                .description("whenPostOneTask task")
                .build();

        //Post
        given()
                .log().all()
                .contentType("application/json")
                .with().body(jsonb.toJson(task))
                .when().post("/customers/" + customer.id + "/tasks")
                .then()
                .statusCode(201);

        //Get by id
        given()
                .when().get("/customers/" + customer.id)
                .then()
                .statusCode(200)
                .assertThat()
                .body(
                        containsString(task.getDescription())
                );

    }

    @Test
    public void whenExceptionThrown_thenTestSucceeds() {
        //List all, should have some entries:
        assertThrows(AssertionError.class, () ->
            given()
                    .when().get("/customers")
                    .then()
                    .statusCode(500)
        );
    }

}
