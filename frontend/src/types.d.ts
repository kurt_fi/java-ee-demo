interface Task {
  id: number
  description: string
  done: boolean
}

interface Customer {
  id: number
  customerId: string
  name: string
  tasks: Task[]
  isExpanded?: boolean
}
