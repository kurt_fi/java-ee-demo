import React from 'react';
import Container from '@material-ui/core/Container';
import Customers from "./Customers";

const App = () => {

  return (
    <Container maxWidth="lg">
      <Customers />
    </Container>
  );
}

export default App
