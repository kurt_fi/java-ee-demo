import React, {useEffect, useState} from "react";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Paper from "@material-ui/core/Paper";
import TableContainer from "@material-ui/core/TableContainer";

const apiUrl = process.env.REACT_APP_BACKEND_URL ? process.env.REACT_APP_BACKEND_URL : 'http://localhost:8080'

interface Row {
  customer: Customer
  isExpanded: boolean
}

const Customers = () => {
  const [refresh, setRefresh] = useState<boolean>(false)
  const [rows, setRows] = useState<Row[]>([])

  useEffect(() => {
    fetch(`${apiUrl}/customers`)
      .then(async response => {
        const customers = await response.json() as Customer[]
        setRows(
          customers.map(customer => ({ customer, isExpanded: false }))
        )
      })
  }, [])

  const setExpanded = (row: Row) => {
    row.isExpanded = !row.isExpanded
    console.log(`Customer ${row.customer.id} clicked and value set to ${row.customer.isExpanded}`)
    // Change state to force re-render
    setRefresh(!refresh)
  }

  return (
    <>
      <h1>Customers</h1>
      <TableContainer component={Paper}>
        <Table aria-label="simple table" className="customer-table">
          <TableHead>
            <TableRow>
              <TableCell>Customer name</TableCell>
              <TableCell>Id</TableCell>
              <TableCell colSpan={2}>Customer id</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <>
                <TableRow key={`customer_${row.customer.id}`} onClick={() => setExpanded(row)} className={`is-link ${row.isExpanded ? 'tasks-row-expanded' : ''}`}>
                  <TableCell component="th" scope="row">
                    {row.customer.name}
                  </TableCell>
                  <TableCell>{row.customer.id}</TableCell>
                  <TableCell>{row.customer.customerId}</TableCell>
                  <TableCell align="right">
                    {row.isExpanded ? (
                      <ExpandLessIcon />
                    ) : (
                      <ExpandMoreIcon />
                    )}
                  </TableCell>
                </TableRow>
                {row.isExpanded && row.customer.tasks && (
                  <TableRow key={`customer_${row.customer.id}_tasks`}>
                    <TableCell colSpan={4}>
                      <Tasks tasks={row.customer.tasks} />
                    </TableCell>
                  </TableRow>
                )}
              </>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}


const Tasks = ({ tasks }: { tasks: Task[] }) => (
  <Table aria-label="simple table" className="tasks-table">
    <TableHead>
      <TableRow>
        <TableCell colSpan={2}>Tasks</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {tasks.map((task, index) => (
        <TableRow key={`task_${task.id} T`}>
          <TableCell component="th" scope="row">
            {task.description}
          </TableCell>
          <TableCell className={`${task.done ? 'task-status-done' : 'task-status-todo'}`}>{task.done ? 'DONE' : 'TODO'}</TableCell>
        </TableRow>
      ))}
    </TableBody>
  </Table>
)

export default Customers
