# JavaEE Selection

## Stakeholders

## Status

No project or code exists yet

## Context

There are many different flavors of JavaEE like Quarkus, Helidon etc. We need to pick one.

## Pros and Cons

### Quarkus

#### Pros

* Based on Eclipse Microprofile (used at company x)
* Based on proven technology (Vert.x)
* Live reload
* GraalVM support (https://github.com/rmh78/quarkus-performance)
* Backed by RedHat

#### Cons

* There is not enough time to do this

### Helidon

#### Pros

* Based on Eclipse Microprofile (used at company x)
* GraalVM support (https://github.com/rmh78/quarkus-performance)
* Backed by Oracle
* (Live reload in next release)

#### Cons

### Helidon

#### Pros

#### Cons

## Decision

Will not spend too much time on this. Quarkus seems cool and GraalVM seems like a game changer.

## Consequences

Considering that the underlying standard is the same, we can easily switch, so no there are no
big consequences. 
